SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Countries]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Countries](
	[CountryId] [nvarchar](128) NOT NULL,
	[CountryCode] [nvarchar](10) NOT NULL,
	[CountryName] [nvarchar](500) NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Table [dbo].[StatesProvinces]    Script Date: 11/9/2014 9:54:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StatesProvinces]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StatesProvinces](
	[StateProvinceId] [nvarchar](128) NOT NULL,
	[CountryId] [nvarchar](128) NOT NULL,
	[StateProvinceCode] [nvarchar](10) NOT NULL,
	[StateName] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_StatesProvinces] PRIMARY KEY CLUSTERED 
(
	[StateProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING ON

GO

/****** Object:  Index [IX_CountryCode]    Script Date: 11/9/2014 9:54:17 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Countries]') AND name = N'IX_CountryCode')
CREATE UNIQUE NONCLUSTERED INDEX [IX_CountryCode] ON [dbo].[Countries]
(
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StatesProvinces_Countries]') AND parent_object_id = OBJECT_ID(N'[dbo].[StatesProvinces]'))
ALTER TABLE [dbo].[StatesProvinces]  WITH CHECK ADD  CONSTRAINT [FK_StatesProvinces_Countries] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Countries] ([CountryId])
ON UPDATE CASCADE
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_StatesProvinces_Countries]') AND parent_object_id = OBJECT_ID(N'[dbo].[StatesProvinces]'))
ALTER TABLE [dbo].[StatesProvinces] CHECK CONSTRAINT [FK_StatesProvinces_Countries]
GO




INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'AL','Alabama');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'AK','Alaska');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'AS','American Samoa');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'AZ','Arizona');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'AR','Arkansas');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'CA','California');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'CO','Colorado');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'CT','Connecticut');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'DE','Delaware');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'DC','District of Columbia');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'FM','Federated States of Micronesia');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'FL','Florida');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'GA','Georgia');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'GU','Guam');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'HI','Hawaii');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'ID','Idaho');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'IL','Illinois');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'IN','Indiana');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'IA','Iowa');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'KS','Kansas');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'KY','Kentucky');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'LA','Louisiana');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'ME','Maine');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'MH','Marshall Islands');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'MD','Maryland');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'MA','Massachusetts');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'MI','Michigan');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'MN','Minnesota');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'MS','Mississippi');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'MO','Missouri');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'MT','Montana');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'NE','Nebraska');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'NV','Nevada');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'NH','New Hampshire');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'NJ','New Jersey');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'NM','New Mexico');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'NY','New York');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'NC','North Carolina');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'ND','North Dakota');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'MP','Northern Mariana Islands');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'OH','Ohio');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'OK','Oklahoma');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'OR','Oregon');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'PW','Palau');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'PA','Pennsylvania');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'PR','Puerto Rico');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'RI','Rhode Island');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'SC','South Carolina');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'SD','South Dakota');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'TN','Tennessee');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'TX','Texas');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'UT','Utah');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'VT','Vermont');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'VI','Virgin Islands');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'VA','Virginia');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'WA','Washington');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'WV','West Virginia');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'WI','Wisconsin');
INSERT INTO StatesProvinces (StateProvinceId, CountryId, StateProvinceCode, StateName) VALUES (NEWID(), (SELECT TOP 1 CountryId FROM Countries WHERE CountryCode = 'US'),'WY','Wyoming');