SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Countries]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Countries](
	[CountryId] [nvarchar](128) NOT NULL,
	[CountryCode] [nvarchar](10) NOT NULL,
	[CountryName] [nvarchar](500) NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING ON

GO

/****** Object:  Index [IX_CountryCode]    Script Date: 11/9/2014 9:53:15 PM ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Countries]') AND name = N'IX_CountryCode')
CREATE UNIQUE NONCLUSTERED INDEX [IX_CountryCode] ON [dbo].[Countries]
(
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'US', 'United States');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CA', 'Canada');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AF', 'Afghanistan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AL', 'Albania');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'DZ', 'Algeria');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'DS', 'American Samoa');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AD', 'Andorra');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AO', 'Angola');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AI', 'Anguilla');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AQ', 'Antarctica');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AG', 'Antigua and/or Barbuda');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AR', 'Argentina');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AM', 'Armenia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AW', 'Aruba');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AU', 'Australia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AT', 'Austria');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AZ', 'Azerbaijan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BS', 'Bahamas');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BH', 'Bahrain');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BD', 'Bangladesh');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BB', 'Barbados');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BY', 'Belarus');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BE', 'Belgium');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BZ', 'Belize');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BJ', 'Benin');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BM', 'Bermuda');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BT', 'Bhutan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BO', 'Bolivia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BA', 'Bosnia and Herzegovina');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BW', 'Botswana');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BV', 'Bouvet Island');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BR', 'Brazil');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'IO', 'British lndian Ocean Territory');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BN', 'Brunei Darussalam');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BG', 'Bulgaria');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BF', 'Burkina Faso');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'BI', 'Burundi');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'KH', 'Cambodia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CM', 'Cameroon');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CV', 'Cape Verde');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'KY', 'Cayman Islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CF', 'Central African Republic');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TD', 'Chad');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CL', 'Chile');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CN', 'China');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CX', 'Christmas Island');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CC', 'Cocos (Keeling) Islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CO', 'Colombia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'KM', 'Comoros');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CG', 'Congo');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CK', 'Cook Islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CR', 'Costa Rica');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'HR', 'Croatia (Hrvatska)');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CU', 'Cuba');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CY', 'Cyprus');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CZ', 'Czech Republic');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'DK', 'Denmark');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'DJ', 'Djibouti');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'DM', 'Dominica');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'DO', 'Dominican Republic');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TP', 'East Timor');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'EC', 'Ecuador');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'EG', 'Egypt');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SV', 'El Salvador');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GQ', 'Equatorial Guinea');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'ER', 'Eritrea');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'EE', 'Estonia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'ET', 'Ethiopia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'FK', 'Falkland Islands (Malvinas)');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'FO', 'Faroe Islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'FJ', 'Fiji');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'FI', 'Finland');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'FR', 'France');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'FX', 'France, Metropolitan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GF', 'French Guiana');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PF', 'French Polynesia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TF', 'French Southern Territories');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GA', 'Gabon');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GM', 'Gambia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GE', 'Georgia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'DE', 'Germany');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GH', 'Ghana');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GI', 'Gibraltar');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GR', 'Greece');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GL', 'Greenland');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GD', 'Grenada');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GP', 'Guadeloupe');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GU', 'Guam');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GT', 'Guatemala');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GN', 'Guinea');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GW', 'Guinea-Bissau');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GY', 'Guyana');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'HT', 'Haiti');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'HM', 'Heard and Mc Donald Islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'HN', 'Honduras');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'HK', 'Hong Kong');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'HU', 'Hungary');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'IS', 'Iceland');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'IN', 'India');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'ID', 'Indonesia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'IR', 'Iran (Islamic Republic of)');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'IQ', 'Iraq');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'IE', 'Ireland');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'IL', 'Israel');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'IT', 'Italy');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CI', 'Ivory Coast');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'JM', 'Jamaica');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'JP', 'Japan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'JO', 'Jordan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'KZ', 'Kazakhstan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'KE', 'Kenya');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'KI', 'Kiribati');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'KP', 'Korea, Democratic People''s Republic of');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'KR', 'Korea, Republic of');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'XK', 'Kosovo');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'KW', 'Kuwait');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'KG', 'Kyrgyzstan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'LA', 'Lao People''s Democratic Republic');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'LV', 'Latvia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'LB', 'Lebanon');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'LS', 'Lesotho');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'LR', 'Liberia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'LY', 'Libyan Arab Jamahiriya');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'LI', 'Liechtenstein');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'LT', 'Lithuania');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'LU', 'Luxembourg');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MO', 'Macau');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MK', 'Macedonia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MG', 'Madagascar');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MW', 'Malawi');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MY', 'Malaysia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MV', 'Maldives');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'ML', 'Mali');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MT', 'Malta');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MH', 'Marshall Islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MQ', 'Martinique');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MR', 'Mauritania');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MU', 'Mauritius');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TY', 'Mayotte');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MX', 'Mexico');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'FM', 'Micronesia, Federated States of');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MD', 'Moldova, Republic of');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MC', 'Monaco');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MN', 'Mongolia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'ME', 'Montenegro');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MS', 'Montserrat');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MA', 'Morocco');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MZ', 'Mozambique');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MM', 'Myanmar');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'NA', 'Namibia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'NR', 'Nauru');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'NP', 'Nepal');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'NL', 'Netherlands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AN', 'Netherlands Antilles');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'NC', 'New Caledonia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'NZ', 'New Zealand');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'NI', 'Nicaragua');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'NE', 'Niger');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'NG', 'Nigeria');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'NU', 'Niue');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'NF', 'Norfork Island');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'MP', 'Northern Mariana Islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'NO', 'Norway');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'OM', 'Oman');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PK', 'Pakistan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PW', 'Palau');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PA', 'Panama');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PG', 'Papua New Guinea');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PY', 'Paraguay');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PE', 'Peru');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PH', 'Philippines');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PN', 'Pitcairn');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PL', 'Poland');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PT', 'Portugal');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PR', 'Puerto Rico');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'QA', 'Qatar');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'RE', 'Reunion');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'RO', 'Romania');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'RU', 'Russian Federation');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'RW', 'Rwanda');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'KN', 'Saint Kitts and Nevis');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'LC', 'Saint Lucia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'VC', 'Saint Vincent and the Grenadines');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'WS', 'Samoa');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SM', 'San Marino');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'ST', 'Sao Tome and Principe');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SA', 'Saudi Arabia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SN', 'Senegal');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'RS', 'Serbia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SC', 'Seychelles');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SL', 'Sierra Leone');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SG', 'Singapore');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SK', 'Slovakia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SI', 'Slovenia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SB', 'Solomon Islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SO', 'Somalia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'ZA', 'South Africa');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GS', 'South Georgia South Sandwich Islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'ES', 'Spain');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'LK', 'Sri Lanka');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SH', 'St. Helena');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'PM', 'St. Pierre and Miquelon');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SD', 'Sudan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SR', 'Suriname');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SJ', 'Svalbarn and Jan Mayen Islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SZ', 'Swaziland');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SE', 'Sweden');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'CH', 'Switzerland');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'SY', 'Syrian Arab Republic');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TW', 'Taiwan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TJ', 'Tajikistan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TZ', 'Tanzania, United Republic of');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TH', 'Thailand');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TG', 'Togo');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TK', 'Tokelau');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TO', 'Tonga');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TT', 'Trinidad and Tobago');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TN', 'Tunisia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TR', 'Turkey');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TM', 'Turkmenistan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TC', 'Turks and Caicos Islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'TV', 'Tuvalu');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'UG', 'Uganda');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'UA', 'Ukraine');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'AE', 'United Arab Emirates');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'GB', 'United Kingdom');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'UM', 'United States minor outlying islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'UY', 'Uruguay');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'UZ', 'Uzbekistan');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'VU', 'Vanuatu');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'VA', 'Vatican City State');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'VE', 'Venezuela');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'VN', 'Vietnam');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'VG', 'Virgin Islands (British)');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'VI', 'Virgin Islands (U.S.)');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'WF', 'Wallis and Futuna Islands');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'EH', 'Western Sahara');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'YE', 'Yemen');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'YU', 'Yugoslavia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'ZR', 'Zaire');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'ZM', 'Zambia');
INSERT INTO countries (CountryId, CountryCode, CountryName) VALUES (NEWID(), 'ZW', 'Zimbabwe');