# README #

This is just a random collection of SQL scripts. Some insert data, others are templates. Mostly for tasks I don't do frequently enough for which I have to spend some time searching. Instead, I put them in one place. Hopefully you'll find them useful too.

### What is this repository for? ###

* Quick summary
* Version

### How do I get set up? ###

There is no setup required. These are individual MS SQL scripts which should run on any database 2012 or later. Probably 2005 or later for most, but I make no guarantees. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact